# Map scale model backlight control
Transmitting a command from the local website to the Arduino board via serial COM port
<br>

##### Scheme of operation

_Website on localhost -> Python forwarder/transceiver module -> Arduino board_
<br>

#### Usage Guide
1. Install the Apache server and configure it

1. Install Python and the python serial module to communicate with the serial port: `pip install pyserial`
1. Place the server configuration file `httpd.conf` in the folder `C:\Apache24\conf` replacing the original one
1. Place the `serial_forwarder.py` file and the web portal with the edited main web page `index.html` in the server folder `C:\Apache24\htdocs`
1. Start or restart the server: `Start\Administrative Tools\Services\Apache` on Windows 7
1. Go to localhost in your browser to check if the site is up and running
<br>

- The control.ino file is used to flash the Arduino board

- The serial COM port number is set in the code of the main/home web page
- The number of ports depends on the number of regions and can be changed if necessary
